=========
pyscholar
=========


.. image:: https://img.shields.io/pypi/v/pyscholar.svg
        :target: https://pypi.python.org/pypi/pyscholar

.. image:: https://img.shields.io/travis/ericgcc/pyscholar.svg
        :target: https://travis-ci.org/ericgcc/pyscholar

.. image:: https://readthedocs.org/projects/pyscholar/badge/?version=latest
        :target: https://pyscholar.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/ericgcc/pyscholar/shield.svg
     :target: https://pyup.io/repos/github/ericgcc/pyscholar/
     :alt: Updates


pyscholar is a python prgramm to scrap Google Scholar data


* Free software: GNU General Public License v3
* Documentation: https://pyscholar.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

