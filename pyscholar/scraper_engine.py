import binascii
import requests
from bs4 import BeautifulSoup
import os

SCHOLAR_SITE = "http://scholar.google.com"
CITATION_VIEW_SUFFIX = "/citations?hl=es&view_op=search_authors&mauthors="


def url_decoder(raw_url):  # TODO: Mejorar esta funcion apestosa
    clear_url = raw_url[16:].strip('\'')
    while clear_url.find('\\x') != -1:
        index = clear_url.find('\\x')
        hex_letter = clear_url[index + 2:index + 4]
        clear_url = clear_url.replace("\\x" + hex_letter,
                                      binascii.unhexlify(bytes(hex_letter, 'utf-8')).decode('utf-8'))
    return clear_url


def authors_parser(soup):
    profiles = []
    if soup.find('div', id='gsc_ccl'):
        soup_profiles = soup.find_all('div', class_='gsc_1usr gs_scl')
        for profile in soup_profiles:
            p = ScholarProfile()
            p['url_img'] = SCHOLAR_SITE + profile.find(class_='gsc_1usr_photo').a.img['src']
            p['url_profile'] = SCHOLAR_SITE + profile.find(class_='gsc_1usr_text').h3.a['href']

            # TODO: Capitalizar bien nombre
            p['name'] = profile.find(class_='gsc_1usr_text').h3.a.string
            p['institution'] = " ".join(profile.find(class_='gsc_1usr_aff').find_all(text=True))
            try:
                # TODO: Eliminar mejor "Citado por "
                p['count_cites'] = profile.find(class_='gsc_1usr_cby').string[11:]

            # TODO: Utilizar una buena excepcion
            except Exception:
                p['count_cites'] = "0"
            profiles.append(p)
        return profiles


def author_documents_parser(soup):
    documents = []
    if soup.find('table', id='gsc_a_t'):
        soup_documents = soup.find_all('tr', class_='gsc_a_tr')
        for document in soup_documents:
            d = Documents()
            d['title'] = document.find(class_="gsc_a_t").a.string
            d['url'] = SCHOLAR_SITE + document.find(class_="gsc_a_t").a['href']

            extra_data = document.find_all(class_="gs_gray")
            d['authors'] = extra_data[0].string

            try:
                d['publication'] = extra_data[1].text
            except Exception:
                d['publication'] = "No publication"

            try:
                d['year'] = document.find(class_="gsc_a_y").span.string
            except Exception:
                d['year'] = "No year"
            documents.append(d)
        return documents


class UrlFactory:
    def __init__(self, site=SCHOLAR_SITE):
        self.site = site
        self.search_pre = ""
        self.keywords = ""

    def profiles(self, keywords):
        """Returns the url of the author citations view for a keyword set"""
        self.search_pre = CITATION_VIEW_SUFFIX
        return self.site + self.search_pre + keywords.replace(' ', '+')

    def next_page(self, soup):
        raw_next_url = soup.find('button', attrs={"aria-label": "Siguiente"})['onclick']
        return self.site + url_decoder(raw_next_url)


class Querier:
    def __init__(self, keywords=""):
        self.query = keywords
        self.profiles = []
        self.citations = []

    def get_all_authors(self, url):
        profiles_url = url.profiles(self.query)
        req = requests.get(profiles_url)
        html = req.content
        return BeautifulSoup(html, 'html.parser')

    def send_request(self, url):
        req = requests.get(url)
        html = req.content
        return BeautifulSoup(html, 'html.parser')

    def get_author_documents(self, url):
        req = requests.get(url)
        html = req.content
        return BeautifulSoup(html, 'html.parser')


class Documents:
    def __init__(self):
        self.attrs = {
            'title': ['', 'Title', 0],
            'url': ['', 'url', 1],
            'authors': ['', 'Authors', 2],
            'publication': ['', 'Publication', 3],
            'year': ['', 'Year', 4]
        }

    def __setitem__(self, key, item):
        if key in self.attrs:
            self.attrs[key][0] = item
        else:
            self.attrs[key] = [item, key, len(self.attrs)]

    def __getitem__(self, key):
        if key in self.attrs:
            return self.attrs[key][0]
        return None

    def __delitem__(self, key):
        if key in self.attrs:
            del self.attrs[key]

    def as_txt(self):
        # Get items sorted in specified order:
        items = sorted(list(self.attrs.values()), key=lambda item: item[2])
        # Find largest label length:
        max_label_len = max([len(str(item[1])) for item in items])
        fmt = '%%%ds: %%s' % max_label_len
        res = []
        for item in items:
            if item[0] is not None:
                res.append(fmt % (item[1], item[0]))
        return '\n'.join(res)


class ScholarProfile:  # Estructura tomada del proyecto original scholar.py
    def __init__(self):
        self.attrs = {
            'name': ['', 'Name', 0],
            'url_profile': ['', 'Profile', 1],
            'url_img': ['', 'Image', 2],
            'institution': ['', 'Institution', 3],
            'count_cites': ['', 'Cites', 4],
        }

    def __setitem__(self, key, item):
        if key in self.attrs:
            self.attrs[key][0] = item
        else:
            self.attrs[key] = [item, key, len(self.attrs)]

    def __getitem__(self, key):
        if key in self.attrs:
            return self.attrs[key][0]
        return None

    def __delitem__(self, key):
        if key in self.attrs:
            del self.attrs[key]

    def as_txt(self):
        # Get items sorted in specified order:
        items = sorted(list(self.attrs.values()), key=lambda item: item[2])
        # Find largest label length:
        max_label_len = max([len(str(item[1])) for item in items])
        fmt = '%%%ds: %%s' % max_label_len
        res = []
        for item in items:
            if item[0] is not None:
                res.append(fmt % (item[1], item[0]))
        return '\n'.join(res)


def main():
    # Page counter
    page = 1

    # Authors counter
    authors = 0

    profiles_url = UrlFactory()

    # Search keywords
    keywords = '''"Instituto de ingenieria" unam'''

    querier = Querier(keywords)

    authors_soup = querier.get_all_authors(profiles_url)

    profiles_list = []

    while True:
        print("============================== PAGE ", page, "==============================")
        profiles = authors_parser(authors_soup)
        profiles_list.extend(profiles)
        authors += len(profiles)

        for profile in profiles:
            print(profile.as_txt() + "\n")

        if authors_soup.find('button', attrs={'aria-label': 'Siguiente'}).has_attr('onclick') is not True:
            break

        authors_soup = querier.send_request(profiles_url.next_page(authors_soup))
        page += 1

    input()
    os.system("cls")
    documents_list = []

    for profile in profiles_list:
        print(profile.as_txt() + "\n")
        print("---------------------")

        documents_soup = querier.get_author_documents(profile['url_profile'])
        page = 1
        cstart = 20
        requested_url = profile['url_profile']
        while True:
            print("=== CITATIONS PAGE ", page, "===")
            print(profile['name'])
            print(requested_url)

            documents = author_documents_parser(documents_soup)
            documents_list.extend(documents)
            for document in documents:
                print(document.as_txt() + "\n")

            if documents_soup.find('button', class_='gs_btnPR gs_in_ib gs_btn_half gs_btn_srt gs_dis') is not None:
                break
            requested_url = profile['url_profile'] + '&hl=es&cstart=' + str(cstart) + '&pagesize=20'
            documents_soup = querier.send_request(requested_url)
            cstart += 20
            page += 1
            # i = input()

    print("Total: ", len(documents_list))


if __name__ == "__main__":
    main()
